﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Drawing;
using System.Windows.Media.Imaging;

namespace AnimalTriviaDrawing
{
    class Animal
    {
        public string phylum;
        public string bioclass;
        public string order;
        public string family;
        public string genus;
        public string species;
        public string appearance;
        public string facts;
        public string name;
        public BitmapImage image;
        
    }
}
