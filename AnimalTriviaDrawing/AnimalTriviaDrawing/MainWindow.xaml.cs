﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Resources;
using System.Collections;
using System.Globalization;
using System.Drawing;
using System.Windows.Resources;


namespace AnimalTriviaDrawing
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Animal> animalList = new List<Animal>();
        Animal alpaca = new Animal();
        Animal vicuna = new Animal();
        Animal wildCamel = new Animal();
        Animal giraffe = new Animal();
        Animal quokka = new Animal();
        Animal cockatiel = new Animal();
        Animal commonStarfish = new Animal();
        Animal currentAnimal = new Animal();
        Random rnd = new Random();
        public string questionLabelContent = "";
        public string appearanceLabelContent = "";
        public string factLabelContent = "";
        public string factBoxContent = "";
        public string appearanceBoxContent = "";
        ScaleTransform expand = new ScaleTransform();
        ScaleTransform contract = new ScaleTransform();
        TransformGroup starButtonsExpand = new TransformGroup();
        TransformGroup starButtonsContract = new TransformGroup();

        public MainWindow()
        {
            InitializeComponent();
            animalList.Add(alpaca);
            animalList.Add(vicuna);
            animalList.Add(wildCamel);
            animalList.Add(giraffe);
            animalList.Add(quokka);
            animalList.Add(cockatiel);
            animalList.Add(commonStarfish);

            alpaca.name = "Alpaca";
            alpaca.species = "Vicugna Pacos";
            alpaca.genus = "Vicugna";
            alpaca.family = "Camelidae";
            alpaca.order = "Artiodactyla";
            alpaca.bioclass = "Mammalia";
            alpaca.phylum = "Chordata";
            alpaca.appearance = "-Quattro gambe\n-Pelliccia bianca lanoso\n-Lungo collo\n-Due dita\n-Lunghezzsa media naso\n-Media coda di lunghezza";
            alpaca.facts = "-Animali sociali\n-Sputa quando si sentono minacciati\n-Denti superiori";
            BitmapImage image = new BitmapImage(new Uri(@"pack://application:,,,/AnimalTriviaDrawing;component/Resources/Alpaca.jpg", UriKind.Absolute));
            alpaca.image = image;
            

            // 6 stars            
            vicuna.name = "Vicuna";
            vicuna.species = "Vicugna Vicugna";
            vicuna.genus = "Vicugna";
            vicuna.family = "Camelidae";
            vicuna.order = "Artiodactyla";
            vicuna.bioclass = "Mammalia";
            vicuna.phylum = "Chordata";
            vicuna.appearance = "-Corpo snello\n-Quattro gambe\n-Luongo collo\n-Lunghe e strette, le orecchie a punta\n-Pelliccia beige con ventre bianco";
            vicuna.facts = "-Cinquanta per cento più pesante di altri mammiferi delle sue dimensioni\n-Cellule del sangue specializzate con dell'emoglobina maggiore affinità per l'ossigeno";
            image = new BitmapImage(new Uri(@"pack://application:,,,/AnimalTriviaDrawing;component/Resources/Vicuna.jpg", UriKind.Absolute));
            vicuna.image = image;

            // 5 stars    
            wildCamel.name = "Wild Camel";
            wildCamel.species = "Camelus ferus";
            wildCamel.genus = "Camelus";
            wildCamel.family = "Camelidae";
            wildCamel.order = "Artiodactyla";
            wildCamel.bioclass = "Mammalia";
            wildCamel.phylum = "Chordata";
            wildCamel.appearance = "-Due piccoli, a forma di piramide gobbe sul dorso\n-Ciglia spesse\n-Quattro gambe\n-Lunga, collo sinuoso";
            wildCamel.facts = "-In pericolo di estinzione\n-Abita a trenta negativo e positivo clima cinquanta gradi";
            image = new BitmapImage(new Uri(@"pack://application:,,,/AnimalTriviaDrawing;component/Resources/WildCamel.jpg", UriKind.Absolute));
            wildCamel.image = image;

            // 4 stars          
            giraffe.name = "Giraffe";
            giraffe.species = "Giraffa camelopardalis";
            giraffe.genus = "Giraffa";
            giraffe.family = "Giraffidae";
            giraffe.order = "Artiodactyla";
            giraffe.bioclass = "Mammalia";
            giraffe.phylum = "Chordata";
            giraffe.appearance = "-Molto lunga, collo dritto\n-Naso lungo\n-Quattro gambe\n-Lunga coda\n-Pelliccia maculata";
            giraffe.facts = "-Vita più alto animale terrestre\n-Può funzionare a 55 chilometri all'ora\n-Lingua nera";
            image = new BitmapImage(new Uri(@"pack://application:,,,/AnimalTriviaDrawing;component/Resources/Giraffe.jpg", UriKind.Absolute));
            giraffe.image = image;

            // 3 stars        
            quokka.name = "Quokka";
            quokka.species = "Setonix Brachyurus";
            quokka.genus = "Setonix";
            quokka.family = "Macropodidae";
            quokka.order = "Diprotodontia";
            quokka.bioclass = "Mammalia";
            quokka.phylum = "Chordata";
            quokka.appearance = "-Orecchie arrotondati\n-Abbastanza piccolo\n-Ha un sacchetto\n-Più lunghe zampe posteriori, zampe anteriori corte\n-Naso a punta";
            quokka.facts = "-Non ha paura degli esseri umani\n-Notturno\n-Può vivere il grasso accumulato nelle loro code per lungo tempo";
            image = new BitmapImage(new Uri(@"pack://application:,,,/AnimalTriviaDrawing;component/Resources/Quokka.jpg", UriKind.Absolute));
            quokka.image = image;

            // 2 stars    
            cockatiel.name = "Cockatiel";
            cockatiel.species = "Nymphicus Hollandicus";
            cockatiel.genus = "Nymphicus";
            cockatiel.family = "Cacatuidae";
            cockatiel.order = "Psittaciformes";
            cockatiel.bioclass = "Aves";
            cockatiel.phylum = "Chordata";
            cockatiel.appearance = "-Guance rosse\n-Lunga coda\n-Lunghe ali\n-Crest di piume gialle in cima alla sua testa\n-Breve, rivolta verso il basso becco";
            cockatiel.facts = "-Molto sociale, andranno depresso se lasciato solo\n-Può vivere per 30 anni\n-Ha un sistema respiratorio sensibile\n-Originario di australia";
            image = new BitmapImage(new Uri(@"pack://application:,,,/AnimalTriviaDrawing;component/Resources/Cockatiel.jpg", UriKind.Absolute));
            cockatiel.image = image;

            // 1 star       
            commonStarfish.name = "Common Starfish";
            commonStarfish.species = "Asterias Rubens";
            commonStarfish.genus = "Asterias";
            commonStarfish.family = "Asteriidae";
            commonStarfish.order = "Forcipulatida";
            commonStarfish.bioclass = "Asteroidea";
            commonStarfish.phylum = "Echinodermata";
            commonStarfish.appearance = "-Cinque arti\n-A forma di stella\n-Solitamente colore arancione\n-Coperto con contundenti, spine bianche";
            commonStarfish.facts = "-Piedi tubo utilizzato per la locomozione e la cattura di prede\n-Carnivoro\n-Ha gli occhi alla fine di ogni braccio\n-Non avere sangue";
            image = new BitmapImage(new Uri(@"pack://application:,,,/AnimalTriviaDrawing;component/Resources/CommonStarfish.jpg", UriKind.Absolute));
            commonStarfish.image = image;

            currentAnimal = alpaca;
            appearanceBox.Text = currentAnimal.appearance;
            appearanceBox.Visibility = Visibility.Visible;
            questionLabel.Visibility = Visibility.Hidden;
            nextButton.Visibility = Visibility.Hidden;
            oneStar.Visibility = Visibility.Hidden;
            twoStars.Visibility = Visibility.Hidden;
            threeStars.Visibility = Visibility.Hidden;
            fourStars.Visibility = Visibility.Hidden;
            fiveStars.Visibility = Visibility.Hidden;
            sixStars.Visibility = Visibility.Hidden;
            ratingLabel.Visibility = Visibility.Hidden;
            factBox.Visibility = Visibility.Hidden;

            expand.ScaleX = 1.5;
            expand.ScaleY = 1.5;
            contract.ScaleX = 0.75;
            contract.ScaleY = 0.75;
            
            starButtonsExpand.Children.Add(expand);
            starButtonsContract.Children.Add(contract);

            fiveStars.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
            fourStars.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
            threeStars.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
            twoStars.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
            oneStar.RenderTransformOrigin = new System.Windows.Point(0.5, 0.5);
        }

        private void revealButton_Click(object sender, RoutedEventArgs e)
        {
            
            factBox.Text = currentAnimal.facts;
            nameBox.Text = currentAnimal.name;
            classificationBox.Text = "Phylum: " + currentAnimal.phylum + "\nBioclass: " + currentAnimal.bioclass + "\nOrder: " + currentAnimal.order + "\nFamily: " + currentAnimal.family + "\nGenus: " + currentAnimal.genus + "\nSpecies: " + currentAnimal.species;
            
            imageBox.Source = currentAnimal.image;

            oneStar.Visibility = Visibility.Visible;
            twoStars.Visibility = Visibility.Visible;
            threeStars.Visibility = Visibility.Visible;
            fourStars.Visibility = Visibility.Visible;
            fiveStars.Visibility = Visibility.Visible;
            sixStars.Visibility = Visibility.Visible;
            ratingLabel.Visibility = Visibility.Visible;
            imageBox.Visibility = Visibility.Visible;
            factBox.Visibility = Visibility.Visible;
            nameBox.Visibility = Visibility.Visible;
            classificationBox.Visibility = Visibility.Visible;
        
            questionLabel.Visibility = Visibility.Visible;
            revealButton.Visibility = Visibility.Hidden;

            
        }

        private void nextButton_Click(object sender, RoutedEventArgs e)
        {
            oneStar.Visibility = Visibility.Hidden;
            twoStars.Visibility = Visibility.Hidden;
            threeStars.Visibility = Visibility.Hidden;
            fourStars.Visibility = Visibility.Hidden;
            fiveStars.Visibility = Visibility.Hidden;
            sixStars.Visibility = Visibility.Hidden;
            nextButton.Visibility = Visibility.Hidden;
            imageBox.Visibility = Visibility.Hidden;
            factBox.Visibility = Visibility.Hidden;
            nameBox.Visibility = Visibility.Hidden;
            questionLabel.Visibility = Visibility.Hidden;
            revealButton.Visibility = Visibility.Visible;
            appearanceBox.Text = currentAnimal.appearance;
            ratingLabel.Visibility = Visibility.Hidden;
            classificationBox.Visibility = Visibility.Hidden;
        }

        private void sixStars_Click(object sender, RoutedEventArgs e)
        {
            foreach (Animal nextAnimal in animalList)
            {
                if (nextAnimal.genus == currentAnimal.genus)
                {
                    currentAnimal = nextAnimal;
                }
            }
            nextButton.Visibility = Visibility.Visible;
        }

        private void fiveStars_Click(object sender, RoutedEventArgs e)
        {
            foreach (Animal nextAnimal in animalList)
            {
                if (nextAnimal.family == currentAnimal.family)
                {
                    currentAnimal = nextAnimal;
                }
            }
            nextButton.Visibility = Visibility.Visible;
        }

        private void fourStars_Click(object sender, RoutedEventArgs e)
        {
            foreach (Animal nextAnimal in animalList)
            {
                if (nextAnimal.order == currentAnimal.order)
                {
                    currentAnimal = nextAnimal;
                }
            }
            nextButton.Visibility = Visibility.Visible;
        }

        private void threeStars_Click(object sender, RoutedEventArgs e)
        {
            foreach (Animal nextAnimal in animalList)
            {
                if (nextAnimal.bioclass == currentAnimal.bioclass)
                {
                    currentAnimal = nextAnimal;
                }
            }
            nextButton.Visibility = Visibility.Visible;
        }

        private void twoStars_Click(object sender, RoutedEventArgs e)
        {
            foreach (Animal nextAnimal in animalList)
            {
                if (nextAnimal.phylum == currentAnimal.phylum)
                {
                    currentAnimal = nextAnimal;
                }
            }
            nextButton.Visibility = Visibility.Visible;
        }

        private void oneStar_Click(object sender, RoutedEventArgs e)
        {
            Animal nextAnimal = animalList[rnd.Next(animalList.Count)];
            currentAnimal = nextAnimal;
            nextButton.Visibility = Visibility.Visible;
        }

        private void questionLabel_Enter(object sender, MouseEventArgs e)
        {
            questionLabelContent = questionLabel.Content.ToString();

            questionLabel.Content = "Does your drawing look something like this?";
        }

        private void questionLabel_Exit(object sender, MouseEventArgs e)
        {
            questionLabel.Content = questionLabelContent;
        }

        private void appearanceLabel_Enter(object sender, MouseEventArgs e)
        {
            appearanceLabelContent = appearanceLabel.Text;
            appearanceLabel.Text = "Appearance";
        }

        private void appearanceLabel_Exit(object sender, MouseEventArgs e)
        {
            appearanceLabel.Text = appearanceLabelContent;
        }

        private void factLabel_Enter(object sender, MouseEventArgs e)
        {
            factLabelContent = factLabel.Text;
            factLabel.Text = "Facts";
        }

        private void factLabel_Exit(object sender, MouseEventArgs e)
        {
            factLabel.Text = factLabelContent;
        }

        private void appearanceBox_Enter(object sender, MouseEventArgs e)
        {
            appearanceBoxContent = appearanceBox.Text;
            appearanceBox.Text = "Translated appearance facts";
        }

        private void appearanceBox_Exit(object sender, MouseEventArgs e)
        {
            appearanceBox.Text = appearanceBoxContent;
        }

        private void factBox_Enter(object sender, MouseEventArgs e)
        {
            factBoxContent = factBox.Text;
            factBox.Text = "Translated trivia facts";
        }

        private void factBox_Exit(object sender, MouseEventArgs e)
        {
            factBox.Text = factBoxContent;
        }

        private void sixStar_Enter(object sender, MouseEventArgs e)
        {
            sixStars.RenderTransform = starButtonsExpand;
            fiveStars.RenderTransform = starButtonsExpand;
            fourStars.RenderTransform = starButtonsExpand;
            threeStars.RenderTransform = starButtonsExpand;
            twoStars.RenderTransform = starButtonsExpand;
            oneStar.RenderTransform = starButtonsExpand;
        }

        private void sixStar_Exit(object sender, MouseEventArgs e)
        {
            sixStars.RenderTransform = starButtonsContract;
            fiveStars.RenderTransform = starButtonsContract;
            fourStars.RenderTransform = starButtonsContract;
            threeStars.RenderTransform = starButtonsContract;
            twoStars.RenderTransform = starButtonsContract;
            oneStar.RenderTransform = starButtonsContract;
        }

        private void fiveStars_Enter(object sender, MouseEventArgs e)
        {
            fiveStars.RenderTransform = starButtonsExpand;
            fourStars.RenderTransform = starButtonsExpand;
            threeStars.RenderTransform = starButtonsExpand;
            twoStars.RenderTransform = starButtonsExpand;
            oneStar.RenderTransform = starButtonsExpand;
        }

        private void fiveStars_Exit(object sender, MouseEventArgs e)
        {
            fiveStars.RenderTransform = starButtonsContract;
            fourStars.RenderTransform = starButtonsContract;
            threeStars.RenderTransform = starButtonsContract;
            twoStars.RenderTransform = starButtonsContract;
            oneStar.RenderTransform = starButtonsContract;
        }

        private void fourStars_Enter(object sender, MouseEventArgs e)
        {
            fourStars.RenderTransform = starButtonsExpand;
            threeStars.RenderTransform = starButtonsExpand;
            twoStars.RenderTransform = starButtonsExpand;
            oneStar.RenderTransform = starButtonsExpand;
        }

        private void fourStars_Exit(object sender, MouseEventArgs e)
        {
            fourStars.RenderTransform = starButtonsContract;
            threeStars.RenderTransform = starButtonsContract;
            twoStars.RenderTransform = starButtonsContract;
            oneStar.RenderTransform = starButtonsContract;
        }

        private void threeStars_Enter(object sender, MouseEventArgs e)
        {
            threeStars.RenderTransform = starButtonsExpand;
            twoStars.RenderTransform = starButtonsExpand;
            oneStar.RenderTransform = starButtonsExpand;
        }

        private void threeStars_Exit(object sender, MouseEventArgs e)
        {
            threeStars.RenderTransform = starButtonsContract;
            twoStars.RenderTransform = starButtonsContract;
            oneStar.RenderTransform = starButtonsContract;
        }

        private void twoStars_Enter(object sender, MouseEventArgs e)
        {
            twoStars.RenderTransform = starButtonsExpand;
            oneStar.RenderTransform = starButtonsExpand;
        }

        private void twoStars_Exit(object sender, MouseEventArgs e)
        {
            twoStars.RenderTransform = starButtonsContract;
            oneStar.RenderTransform = starButtonsContract;
        }

        private void oneStar_Enter(object sender, MouseEventArgs e)
        {
            oneStar.RenderTransform = starButtonsExpand;
        }

        private void oneStar_Exit(object sender, MouseEventArgs e)
        {
            oneStar.RenderTransform = starButtonsContract;
        }


        

    }
}
